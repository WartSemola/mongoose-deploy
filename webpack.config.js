let path = require('path');
let nodeExternals = require('webpack-node-externals');
let CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  entry: './src/index.ts',
  devtool: 'inline-source-map',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js'
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".tsx", ".js"]
  },
  mode: 'development',
  module: {
    rules: [
      {
        use: 'ts-loader',
        test: /\.ts?$/
      }
    ]
  },
  target: 'node',
  externals: [nodeExternals()],
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    dns: 'empty'
  },
  plugins: [
    new CopyWebpackPlugin([
      {from: 'src/assets', to: 'assets'},
      {from: 'frontend/dist', to: 'fe-dist'}
    ])
  ]
}