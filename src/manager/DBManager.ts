/// <reference path="../typings/tsd.d.ts" />
import ConfigurationManager from '../manager/ConfigurationManager';
import * as MySQL from 'mysql';


class DBManager {
    private pool: MySQL.Pool;
    private static instance: DBManager;
    private dbConf: any = {};
    private mysqlConf: any = {};
    private keepAliveInterval: any;

    public init(): void {
        this.mysqlConf = ConfigurationManager.getInstance().getConfiguration()["startupConfiguration"]["mysql"];
        this.dbConf = {
            connectionLimit: this.mysqlConf.connectionLimit,
            host: this.mysqlConf.host,
            port: this.mysqlConf.port,
            user: this.mysqlConf.user,
            password: this.mysqlConf.password,
            database: this.mysqlConf.db
        };

        if (this.mysqlConf.enabled == true)
            this.connect();
        else
            console.log("[DBManager]", "db connection disabled in configuration")
    }

    private connect(): void {
        let that = this;

        if (this.pool == undefined) {
            this.pool = MySQL.createPool(this.dbConf);
        }

        this.pool.getConnection(function (err, connection) {
            if (err) {
                console.error(new Date() + ' | [DBManager]', 'error connecting: ' + err.stack);
                return;
            }
            that.keepAliveInterval = setInterval(that.keepalive, that.mysqlConf.keepAliveTime);
            console.log(new Date() + ' | [DBManager]', 'connected as id ' + connection.threadId);
        });

        this.pool.on('error', function (err) {
            console.log(new Date() + ' | [DBManager]', 'error : ' + err.code);
            if (err.code == 'PROTOCOL_CONNECTION_LOST') {
                clearInterval(that.keepAliveInterval);
                console.log(new Date() + ' | [DBManager]', 'Reconnection...');
                that.connect();
            }
        });
    }

    public disconnect(): void {
        if (this.pool != undefined) {
            this.pool.end();
            console.log(new Date() + " | [DBManager]", "disconnected");
            this.pool = undefined;
        }
    }

    private keepalive(): void {
        let that = this;
        console.log("Keep Alive")
        this.pool.query("SELECT 1", function (error, results, fields) {
            if (error) {
                console.log(new Date() + ' | [DBManager]', 'keepalive error : ' + error.code);
                that.connect();
                console.log(new Date() + ' | [DBManager]', 'Reconnection...');
            }
            console.log(new Date() + " | [DBManager] KeepAlive: OK");
        })
    }

    public static getInstance() {
        return this.instance || (this.instance = new this());
    }
}

export default DBManager;