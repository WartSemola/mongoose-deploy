//import * as fs from 'fs'

class ConfigurationManager{
    private static instance:ConfigurationManager;

    private configuration:any = {
        "startupConfiguration": {
            "server": {
                "host": "127.0.0.1",
                "port": "8000"
            },
            "redis": {
                "host": "localhost",
                "port": 6379,
                "db": 5
            },
            "mysql": {
                "enabled": true,
                "connectionLimit": 10,
                "keepAliveTime": 600000,
                "host": "eu-cdbr-west-02.cleardb.net",
                "port": 3306,
                "user": "bcae5bd6d64c10",
                "password": "67bc93b4",
                "db": "heroku_53b17987c16e8c0"
            }
        }
    };
    private configurationFilePath:string = "../configuration/conf.json";

    public readConfiguration(): void {
        console.log("[ConfigurationManager]", "readConfiguration");
        //if(this.configurationFilePath == undefined){console.log(new Date() + " | [ConfigurationManager] no conf path");}
        //this.configuration = require(this.configurationFilePath);
    }

    public reloadConfiguration(): void {
        console.log("[ConfigurationManager]", "reloadConfiguration");
        //this.configuration = JSON.parse(fs.readFileSync("../configuration/config.json", null).toString());
    }

    public getConfiguration(): any{
        return this.configuration;
    }

    public static getInstance(){
        return this.instance || (this.instance = new this());
    }
}

export default ConfigurationManager;