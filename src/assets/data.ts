export const showcaseMock = [
    {
        "id": 1,
        "creationDate": 1523969323070,
        "name": "MyTest",
        "vatNumber": "VAT-1234234",
        "socialData": {
            "website": "https://www.google.it",
            "phone": "063123123",
            "cellphone": "32323232",
            "facebookLink": "https://www.facebook.com",
            "email": "mytest@mailinator.com",
            "tripadvisorLink": "http://www.tripadvisor.it",
            "fax": "123123123"
        },
        "rating": {
            "averageValue": 4,
            "ratingCount": 10,
            "review_rating_count": {
                "1": "0",
                "2": "1",
                "3": "3",
                "4": "1",
                "5": "5"
            }
        },
        "imageList": [
            {
                "name": "photo-1",
                "path": "http://www.sample-videos.com/img/Sample-jpg-image-1mb.jpg"
            },
            {
                "name": "photo-2",
                "path": "http://www.sample-videos.com/img/Sample-jpg-image-1mb.jpg"
            }
        ],
        "place": {
            "streetName": "via Roma",
            "streetNumber": "12",
            "zipCode": "01010",
            "country": "Italia",
            "state": "Viterbo",
            "city": "Lazio"
        }
    }
]
