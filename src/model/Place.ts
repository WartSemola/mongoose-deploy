interface Place{
    streetName: string;
    streetNumber: string;
    zipCode: string;
    country: string;
    state: string;
    city: string;
}
export = Place;