import Place = require("./Place");
import Image = require("./Image");
import SocialData = require("./SocialData");

interface User {
    id: string;
    name: string;
    email: string;
    creationDate: string;
    socialData: SocialData;
    imageList: Array<Image>;
    place: Place;
}
export = User;