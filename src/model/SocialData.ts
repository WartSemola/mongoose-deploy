interface SocialData {
    website: string;
    facebookLink: string;
    tripadvisorLink: string;
    phone: string;
    cellphone: string;
    fax: string;
    email: string;
}
export = SocialData;