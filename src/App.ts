import express = require('express')
import bodyParser = require('body-parser');
import logger = require('morgan');
import cors = require('cors');
import ConfigurationManager from './manager/ConfigurationManager';
import DBManager from './manager/DBManager';
import path = require('path')

import PingRouter from './routes/PingRouter';
import ReloadConfigurationRouter from "./routes/ReloadConfigurationRouter";
import RatingRouter from "./routes/RatingRouter";
import EntityRouter from "./routes/EntityRouter";
import UploadRouter from "./routes/UploadRouter";

// let express = require('express');


// Creates and configures an ExpressJS web server.
class App {

  // ref to Express instance
  public express: express.Application;
  // public express:any;

  //Run configuration methods on the Express instance.
  constructor() {
    console.log("start server")
    this.express = exports = express();
    this.middleware();
    this.routes();
    ConfigurationManager.getInstance().readConfiguration();
    // DBManager.getInstance().init();
  }

  // Configure Express middleware.
  private middleware(): void {
    // this.express.use(express.static("./build/fe-dist"))

    this.express.use(logger('dev'));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({extended: false}));
    this.express.use(cors());
    //     var fs = require('fs');
//     fs.readdir("./build/fe-dist", (err, files) => {
//       files.forEach(file => {
//         console.log(file);
//       });
//     })
// console.log(fs.existsSync(__dirname +"/assets"))
  }

  // Configure API endpoints.
  private routes(): void {
    /* This is just to get up and running, and to make sure what we've got is
     * working so far. This function will change when we start to add more
     * API endpoints */
    let router = express.Router();
    // placeholder route handler
    router.get('/', (req: any, res: any, next: any) => {
      res.json({
        message: 'Hello World!'
      });
    });
    this.express.use('/', router);
    this.express.use('/api/v1/ping', PingRouter);
    this.express.use('/api/v1/reloadConfiguration', ReloadConfigurationRouter);
    this.express.use('/api/v1/rating', RatingRouter);
    this.express.use('/api/v1/entity', EntityRouter);
    this.express.use('/upload-manager', UploadRouter);
    this.express.use('/pwa', express.static("./build/fe-dist"));
    // this.express.use('/pwa', PwaRouter);
    console.log("###########################   DirName     ###########################   ", __dirname)
    // this.express.use('/pwa', PwaRouter)
  }

}

export default new App().express;
