import {Router, Request, Response, NextFunction} from 'express';
const Entities = require('../assets/data');

export class EntityRouter {
    router: Router

    /**
     * Initialize the EntityRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    /**
     * POST new Entity.
     */
    public addEntity(req: Request, res: Response, next: NextFunction) {

        let vatNumber = req.body.vatNumber;
        let name = req.body.name;
        let socialData = {
            "website": req.body.website,
            "phone": req.body.phone,
            "cellphone": req.body.cellphone
        };

        console.log("[Rating]", "adding entity with vatNumber=" + vatNumber + " and name=" + name);

        //check if alredy exists entity with given vatNumber

        res.json({
            status: 'ok'
        });
    }

    /**
     * GET one entity by id
     */
    public getEntity(req: Request, res: Response, next: NextFunction) {
        let entityId = parseInt(req.params.entityId);
        let entity = Entities.find((entity:any) => entity.id === entityId);
        if (entity) {
            res.status(200)
                .send({
                    message: 'Success',
                    status: res.status,
                    entity: entity
                });
        }
        else {
            res.status(404)
                .send({
                    message: 'No entity found with the given id.',
                    status: res.status
                });
        }
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/', this.addEntity);
        this.router.get('/:entityId', this.getEntity);
    }

}

// Create the HeroRouter, and export its configured Express.Router
const entityRouter = new EntityRouter();
entityRouter.init();

let _entityRouter = entityRouter.router;

export default _entityRouter;
// export default entityRouter;
