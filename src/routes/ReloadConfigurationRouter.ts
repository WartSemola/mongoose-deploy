import {Router, Request, Response, NextFunction} from 'express';
import ConfigurationManager from '../manager/ConfigurationManager';

export class ReloadConfigurationRouter {
    router: Router;

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    /**
     * GET all Heroes.
     */
    public reload(req: Request, res: Response, next: NextFunction) {
        ConfigurationManager.getInstance().reloadConfiguration();
        res.json({
            status: 'ok'
        });
    }


    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/', this.reload);
    }

}

// Create the HeroRouter, and export its configured Express.Router
let reloadConfigurationRouter = new ReloadConfigurationRouter();
reloadConfigurationRouter.init();
let _reloadConfigurationRouter = reloadConfigurationRouter.router;
// export default reloadConfigurationRouter;
export default _reloadConfigurationRouter;
