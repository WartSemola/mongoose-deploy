import {Router, Request, Response, NextFunction} from 'express';
import formidable = require("formidable");
import fs = require("fs");
import path = require('path')


export class UploadRouter {
  router: Router

  /**
   * Initialize the HeroRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }

  /**
   * GET all Heroes.
   */
  public uploadImage(req: Request, res: Response, next: NextFunction) {
    let form = new formidable.IncomingForm();


    form.parse(req, (err: Error, fields: formidable.Fields, files: formidable.Files) => {
      var oldpath = files.video.path;
      console.log(oldpath)

      var newpath = path.resolve('./uploadFolder') + "/" + files.video.name;


      fs.rename(oldpath, newpath, function (err) {
        console.log("Error ", err)
        if (err) {
          res.status(500).send({message: 'Failed', status: res.status})
          throw err;
        }
        // res.write('File uploaded and moved!');
        res.status(200)
          .send({
            message: 'Success',
            status: res.status
          });
      });
    })

  }


  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.post('/image', this.uploadImage);
  }

}

// Create the HeroRouter, and export its configured Express.Router
const uploadRouter = new UploadRouter();
uploadRouter.init();
let _uploadRouter = uploadRouter.router
export default _uploadRouter;
