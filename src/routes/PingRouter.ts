import {Router, Request, Response, NextFunction} from 'express';

export class PingRouter {
  router: Router

  /**
   * Initialize the HeroRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }

  /**
   * GET all Heroes.
   */
  public ping(req: Request, res: Response, next: NextFunction) {
      res.json({
          status: 'ok'
      });
  }


  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.get('/', this.ping);
  }

}

// Create the HeroRouter, and export its configured Express.Router
const pingRoutes = new PingRouter();
pingRoutes.init();
let _pingRoutes = pingRoutes.router
export default _pingRoutes;
// export default pingRoutes;
