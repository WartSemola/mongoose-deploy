import {Router, Request, Response, NextFunction} from 'express';
const Entities = require('../assets/data');

export class RatingRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    /**
     * GET all Heroes.
     */
    public rate(req: Request, res: Response, next: NextFunction) {
        let userId = req.body.userId;
        let entityId = req.body.entityId;
        let ratingValue = req.body.ratingValue;

        console.log("[Rating]", "User with id=" + userId + " rate " + ratingValue + " star entity with id=" + entityId);

        res.json({
            status: 'ok'
        });
    }

    /**
     * GET one hero by id
     */
    public getRating(req: Request, res: Response, next: NextFunction) {
        let entityId = parseInt(req.params.entityId);
        let entity = Entities.find((entity:any) => entity.id === entityId);
        if (entity) {
            res.status(200)
                .send({
                    message: 'Success',
                    status: res.status,
                    entity: entity["rating"]
                });
        }
        else {
            res.status(404)
                .send({
                    message: 'No entity found with the given id.',
                    status: res.status
                });
        }
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/', this.rate);
        this.router.get('/:entityId', this.getRating);
    }

}

// Create the HeroRouter, and export its configured Express.Router
const ratingRouter = new RatingRouter();
ratingRouter.init();
let _ratingRouter = ratingRouter.router;

export default _ratingRouter;
// export default ratingRouter;
