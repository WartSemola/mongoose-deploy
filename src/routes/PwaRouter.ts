import {NextFunction, Request, Response, Router} from "express";
import express = require('express')
import path = require('path');


export class PwaRouter {
  router: Router
  public express: any;


  constructor() {
    this.router = Router();
    this.express = express();
  }

  public getPwaFE(req: Request, res: Response, next: NextFunction) {


  }

  init() {
    this.router.get('/', ((req, res, next) => {
      this.express.use(express.static("./build/fe-dist/"));

      // res.sendFile(path.join(__dirname + 'assets/data.ts'))
    }));

  }
}

const pwaRouter = new PwaRouter();
pwaRouter.init();

let _entityRouter = pwaRouter.router;

export default _entityRouter;