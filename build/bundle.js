/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/App.ts":
/*!********************!*\
  !*** ./src/App.ts ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {
Object.defineProperty(exports, "__esModule", { value: true });
var express = __webpack_require__(/*! express */ "express");
var bodyParser = __webpack_require__(/*! body-parser */ "body-parser");
var logger = __webpack_require__(/*! morgan */ "morgan");
var cors = __webpack_require__(/*! cors */ "cors");
var ConfigurationManager_1 = __webpack_require__(/*! ./manager/ConfigurationManager */ "./src/manager/ConfigurationManager.ts");
var PingRouter_1 = __webpack_require__(/*! ./routes/PingRouter */ "./src/routes/PingRouter.ts");
var ReloadConfigurationRouter_1 = __webpack_require__(/*! ./routes/ReloadConfigurationRouter */ "./src/routes/ReloadConfigurationRouter.ts");
var RatingRouter_1 = __webpack_require__(/*! ./routes/RatingRouter */ "./src/routes/RatingRouter.ts");
var EntityRouter_1 = __webpack_require__(/*! ./routes/EntityRouter */ "./src/routes/EntityRouter.ts");
var UploadRouter_1 = __webpack_require__(/*! ./routes/UploadRouter */ "./src/routes/UploadRouter.ts");
// let express = require('express');
// Creates and configures an ExpressJS web server.
var App = /** @class */ (function () {
    // public express:any;
    //Run configuration methods on the Express instance.
    function App() {
        console.log("start server");
        this.express = exports = express();
        this.middleware();
        this.routes();
        ConfigurationManager_1.default.getInstance().readConfiguration();
        // DBManager.getInstance().init();
    }
    // Configure Express middleware.
    App.prototype.middleware = function () {
        // this.express.use(express.static("./build/fe-dist"))
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(cors());
        //     var fs = require('fs');
        //     fs.readdir("./build/fe-dist", (err, files) => {
        //       files.forEach(file => {
        //         console.log(file);
        //       });
        //     })
        // console.log(fs.existsSync(__dirname +"/assets"))
    };
    // Configure API endpoints.
    App.prototype.routes = function () {
        /* This is just to get up and running, and to make sure what we've got is
         * working so far. This function will change when we start to add more
         * API endpoints */
        var router = express.Router();
        // placeholder route handler
        router.get('/', function (req, res, next) {
            res.json({
                message: 'Hello World!'
            });
        });
        this.express.use('/', router);
        this.express.use('/api/v1/ping', PingRouter_1.default);
        this.express.use('/api/v1/reloadConfiguration', ReloadConfigurationRouter_1.default);
        this.express.use('/api/v1/rating', RatingRouter_1.default);
        this.express.use('/api/v1/entity', EntityRouter_1.default);
        this.express.use('/upload-manager', UploadRouter_1.default);
        this.express.use('/pwa', express.static("./build/fe-dist"));
        // this.express.use('/pwa', PwaRouter);
        console.log("###########################   DirName     ###########################   ", __dirname);
        // this.express.use('/pwa', PwaRouter)
    };
    return App;
}());
exports.default = new App().express;

/* WEBPACK VAR INJECTION */}.call(this, "/"))

/***/ }),

/***/ "./src/assets/data.ts":
/*!****************************!*\
  !*** ./src/assets/data.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.showcaseMock = [
    {
        "id": 1,
        "creationDate": 1523969323070,
        "name": "MyTest",
        "vatNumber": "VAT-1234234",
        "socialData": {
            "website": "https://www.google.it",
            "phone": "063123123",
            "cellphone": "32323232",
            "facebookLink": "https://www.facebook.com",
            "email": "mytest@mailinator.com",
            "tripadvisorLink": "http://www.tripadvisor.it",
            "fax": "123123123"
        },
        "rating": {
            "averageValue": 4,
            "ratingCount": 10,
            "review_rating_count": {
                "1": "0",
                "2": "1",
                "3": "3",
                "4": "1",
                "5": "5"
            }
        },
        "imageList": [
            {
                "name": "photo-1",
                "path": "http://www.sample-videos.com/img/Sample-jpg-image-1mb.jpg"
            },
            {
                "name": "photo-2",
                "path": "http://www.sample-videos.com/img/Sample-jpg-image-1mb.jpg"
            }
        ],
        "place": {
            "streetName": "via Roma",
            "streetNumber": "12",
            "zipCode": "01010",
            "country": "Italia",
            "state": "Viterbo",
            "city": "Lazio"
        }
    }
];


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var http = __webpack_require__(/*! http */ "http");
var debug = __webpack_require__(/*! debug */ "debug");
var App_1 = __webpack_require__(/*! ./App */ "./src/App.ts");
var port = normalizePort(process.env.PORT || 3000);
App_1.default.set('port', port);
var server = http.createServer(App_1.default);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
function normalizePort(val) {
    var port = (typeof val === 'string') ? parseInt(val, 10) : val;
    if (isNaN(port))
        return val;
    else if (port >= 0)
        return port;
    else
        return false;
}
function onError(error) {
    if (error.syscall !== 'listen')
        throw error;
    var bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port;
    switch (error.code) {
        case 'EACCES':
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}
function onListening() {
    var addr = server.address();
    var bind = (typeof addr === 'string') ? "pipe " + addr : "port " + addr.port;
    console.log(bind);
    debug("Listening on " + bind);
}


/***/ }),

/***/ "./src/manager/ConfigurationManager.ts":
/*!*********************************************!*\
  !*** ./src/manager/ConfigurationManager.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

//import * as fs from 'fs'
Object.defineProperty(exports, "__esModule", { value: true });
var ConfigurationManager = /** @class */ (function () {
    function ConfigurationManager() {
        this.configuration = {
            "startupConfiguration": {
                "server": {
                    "host": "127.0.0.1",
                    "port": "8000"
                },
                "redis": {
                    "host": "localhost",
                    "port": 6379,
                    "db": 5
                },
                "mysql": {
                    "enabled": true,
                    "connectionLimit": 10,
                    "keepAliveTime": 600000,
                    "host": "eu-cdbr-west-02.cleardb.net",
                    "port": 3306,
                    "user": "bcae5bd6d64c10",
                    "password": "67bc93b4",
                    "db": "heroku_53b17987c16e8c0"
                }
            }
        };
        this.configurationFilePath = "../configuration/conf.json";
    }
    ConfigurationManager.prototype.readConfiguration = function () {
        console.log("[ConfigurationManager]", "readConfiguration");
        //if(this.configurationFilePath == undefined){console.log(new Date() + " | [ConfigurationManager] no conf path");}
        //this.configuration = require(this.configurationFilePath);
    };
    ConfigurationManager.prototype.reloadConfiguration = function () {
        console.log("[ConfigurationManager]", "reloadConfiguration");
        //this.configuration = JSON.parse(fs.readFileSync("../configuration/config.json", null).toString());
    };
    ConfigurationManager.prototype.getConfiguration = function () {
        return this.configuration;
    };
    ConfigurationManager.getInstance = function () {
        return this.instance || (this.instance = new this());
    };
    return ConfigurationManager;
}());
exports.default = ConfigurationManager;


/***/ }),

/***/ "./src/routes/EntityRouter.ts":
/*!************************************!*\
  !*** ./src/routes/EntityRouter.ts ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __webpack_require__(/*! express */ "express");
var Entities = __webpack_require__(/*! ../assets/data */ "./src/assets/data.ts");
var EntityRouter = /** @class */ (function () {
    /**
     * Initialize the EntityRouter
     */
    function EntityRouter() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * POST new Entity.
     */
    EntityRouter.prototype.addEntity = function (req, res, next) {
        var vatNumber = req.body.vatNumber;
        var name = req.body.name;
        var socialData = {
            "website": req.body.website,
            "phone": req.body.phone,
            "cellphone": req.body.cellphone
        };
        console.log("[Rating]", "adding entity with vatNumber=" + vatNumber + " and name=" + name);
        //check if alredy exists entity with given vatNumber
        res.json({
            status: 'ok'
        });
    };
    /**
     * GET one entity by id
     */
    EntityRouter.prototype.getEntity = function (req, res, next) {
        var entityId = parseInt(req.params.entityId);
        var entity = Entities.find(function (entity) { return entity.id === entityId; });
        if (entity) {
            res.status(200)
                .send({
                message: 'Success',
                status: res.status,
                entity: entity
            });
        }
        else {
            res.status(404)
                .send({
                message: 'No entity found with the given id.',
                status: res.status
            });
        }
    };
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    EntityRouter.prototype.init = function () {
        this.router.post('/', this.addEntity);
        this.router.get('/:entityId', this.getEntity);
    };
    return EntityRouter;
}());
exports.EntityRouter = EntityRouter;
// Create the HeroRouter, and export its configured Express.Router
var entityRouter = new EntityRouter();
entityRouter.init();
var _entityRouter = entityRouter.router;
exports.default = _entityRouter;
// export default entityRouter;


/***/ }),

/***/ "./src/routes/PingRouter.ts":
/*!**********************************!*\
  !*** ./src/routes/PingRouter.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __webpack_require__(/*! express */ "express");
var PingRouter = /** @class */ (function () {
    /**
     * Initialize the HeroRouter
     */
    function PingRouter() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * GET all Heroes.
     */
    PingRouter.prototype.ping = function (req, res, next) {
        res.json({
            status: 'ok'
        });
    };
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    PingRouter.prototype.init = function () {
        this.router.get('/', this.ping);
    };
    return PingRouter;
}());
exports.PingRouter = PingRouter;
// Create the HeroRouter, and export its configured Express.Router
var pingRoutes = new PingRouter();
pingRoutes.init();
var _pingRoutes = pingRoutes.router;
exports.default = _pingRoutes;
// export default pingRoutes;


/***/ }),

/***/ "./src/routes/RatingRouter.ts":
/*!************************************!*\
  !*** ./src/routes/RatingRouter.ts ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __webpack_require__(/*! express */ "express");
var Entities = __webpack_require__(/*! ../assets/data */ "./src/assets/data.ts");
var RatingRouter = /** @class */ (function () {
    /**
     * Initialize the HeroRouter
     */
    function RatingRouter() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * GET all Heroes.
     */
    RatingRouter.prototype.rate = function (req, res, next) {
        var userId = req.body.userId;
        var entityId = req.body.entityId;
        var ratingValue = req.body.ratingValue;
        console.log("[Rating]", "User with id=" + userId + " rate " + ratingValue + " star entity with id=" + entityId);
        res.json({
            status: 'ok'
        });
    };
    /**
     * GET one hero by id
     */
    RatingRouter.prototype.getRating = function (req, res, next) {
        var entityId = parseInt(req.params.entityId);
        var entity = Entities.find(function (entity) { return entity.id === entityId; });
        if (entity) {
            res.status(200)
                .send({
                message: 'Success',
                status: res.status,
                entity: entity["rating"]
            });
        }
        else {
            res.status(404)
                .send({
                message: 'No entity found with the given id.',
                status: res.status
            });
        }
    };
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    RatingRouter.prototype.init = function () {
        this.router.post('/', this.rate);
        this.router.get('/:entityId', this.getRating);
    };
    return RatingRouter;
}());
exports.RatingRouter = RatingRouter;
// Create the HeroRouter, and export its configured Express.Router
var ratingRouter = new RatingRouter();
ratingRouter.init();
var _ratingRouter = ratingRouter.router;
exports.default = _ratingRouter;
// export default ratingRouter;


/***/ }),

/***/ "./src/routes/ReloadConfigurationRouter.ts":
/*!*************************************************!*\
  !*** ./src/routes/ReloadConfigurationRouter.ts ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __webpack_require__(/*! express */ "express");
var ConfigurationManager_1 = __webpack_require__(/*! ../manager/ConfigurationManager */ "./src/manager/ConfigurationManager.ts");
var ReloadConfigurationRouter = /** @class */ (function () {
    /**
     * Initialize the HeroRouter
     */
    function ReloadConfigurationRouter() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * GET all Heroes.
     */
    ReloadConfigurationRouter.prototype.reload = function (req, res, next) {
        ConfigurationManager_1.default.getInstance().reloadConfiguration();
        res.json({
            status: 'ok'
        });
    };
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    ReloadConfigurationRouter.prototype.init = function () {
        this.router.get('/', this.reload);
    };
    return ReloadConfigurationRouter;
}());
exports.ReloadConfigurationRouter = ReloadConfigurationRouter;
// Create the HeroRouter, and export its configured Express.Router
var reloadConfigurationRouter = new ReloadConfigurationRouter();
reloadConfigurationRouter.init();
var _reloadConfigurationRouter = reloadConfigurationRouter.router;
// export default reloadConfigurationRouter;
exports.default = _reloadConfigurationRouter;


/***/ }),

/***/ "./src/routes/UploadRouter.ts":
/*!************************************!*\
  !*** ./src/routes/UploadRouter.ts ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __webpack_require__(/*! express */ "express");
var formidable = __webpack_require__(/*! formidable */ "formidable");
var fs = __webpack_require__(/*! fs */ "fs");
var path = __webpack_require__(/*! path */ "path");
var UploadRouter = /** @class */ (function () {
    /**
     * Initialize the HeroRouter
     */
    function UploadRouter() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * GET all Heroes.
     */
    UploadRouter.prototype.uploadImage = function (req, res, next) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            var oldpath = files.video.path;
            console.log(oldpath);
            var newpath = path.resolve('./uploadFolder') + "/" + files.video.name;
            fs.rename(oldpath, newpath, function (err) {
                console.log("Error ", err);
                if (err) {
                    res.status(500).send({ message: 'Failed', status: res.status });
                    throw err;
                }
                // res.write('File uploaded and moved!');
                res.status(200)
                    .send({
                    message: 'Success',
                    status: res.status
                });
            });
        });
    };
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    UploadRouter.prototype.init = function () {
        this.router.post('/image', this.uploadImage);
    };
    return UploadRouter;
}());
exports.UploadRouter = UploadRouter;
// Create the HeroRouter, and export its configured Express.Router
var uploadRouter = new UploadRouter();
uploadRouter.init();
var _uploadRouter = uploadRouter.router;
exports.default = _uploadRouter;


/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),

/***/ "debug":
/*!************************!*\
  !*** external "debug" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("debug");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "formidable":
/*!*****************************!*\
  !*** external "formidable" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("formidable");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),

/***/ "morgan":
/*!*************************!*\
  !*** external "morgan" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("morgan");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL0FwcC50cyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2RhdGEudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LnRzIiwid2VicGFjazovLy8uL3NyYy9tYW5hZ2VyL0NvbmZpZ3VyYXRpb25NYW5hZ2VyLnRzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvRW50aXR5Um91dGVyLnRzIiwid2VicGFjazovLy8uL3NyYy9yb3V0ZXMvUGluZ1JvdXRlci50cyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL1JhdGluZ1JvdXRlci50cyIsIndlYnBhY2s6Ly8vLi9zcmMvcm91dGVzL1JlbG9hZENvbmZpZ3VyYXRpb25Sb3V0ZXIudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy9VcGxvYWRSb3V0ZXIudHMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiYm9keS1wYXJzZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJjb3JzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZGVidWdcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJleHByZXNzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZm9ybWlkYWJsZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcImZzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiaHR0cFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm1vcmdhblwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInBhdGhcIiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7O0FDbkVBLDREQUFtQztBQUNuQyx1RUFBMkM7QUFDM0MseURBQWtDO0FBQ2xDLG1EQUE4QjtBQUM5QixnSUFBa0U7QUFJbEUsZ0dBQTZDO0FBQzdDLDZJQUEyRTtBQUMzRSxzR0FBaUQ7QUFDakQsc0dBQWlEO0FBQ2pELHNHQUFpRDtBQUVqRCxvQ0FBb0M7QUFHcEMsa0RBQWtEO0FBQ2xEO0lBSUUsc0JBQXNCO0lBRXRCLG9EQUFvRDtJQUNwRDtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDO1FBQzNCLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxHQUFHLE9BQU8sRUFBRSxDQUFDO1FBQ25DLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDZCw4QkFBb0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQ3ZELGtDQUFrQztJQUNwQyxDQUFDO0lBRUQsZ0NBQWdDO0lBQ3hCLHdCQUFVLEdBQWxCO1FBQ0Usc0RBQXNEO1FBRXRELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsRUFBQyxRQUFRLEVBQUUsS0FBSyxFQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7UUFDekIsOEJBQThCO1FBQ2xDLHNEQUFzRDtRQUN0RCxnQ0FBZ0M7UUFDaEMsNkJBQTZCO1FBQzdCLFlBQVk7UUFDWixTQUFTO1FBQ1QsbURBQW1EO0lBQ2pELENBQUM7SUFFRCwyQkFBMkI7SUFDbkIsb0JBQU0sR0FBZDtRQUNFOzsyQkFFbUI7UUFDbkIsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzlCLDRCQUE0QjtRQUM1QixNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxVQUFDLEdBQVEsRUFBRSxHQUFRLEVBQUUsSUFBUztZQUM1QyxHQUFHLENBQUMsSUFBSSxDQUFDO2dCQUNQLE9BQU8sRUFBRSxjQUFjO2FBQ3hCLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxvQkFBVSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUUsbUNBQXlCLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxzQkFBWSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsc0JBQVksQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLHNCQUFZLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7UUFDNUQsdUNBQXVDO1FBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEVBQTBFLEVBQUUsU0FBUyxDQUFDO1FBQ2xHLHNDQUFzQztJQUN4QyxDQUFDO0lBRUgsVUFBQztBQUFELENBQUM7QUFFRCxrQkFBZSxJQUFJLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQzs7Ozs7Ozs7Ozs7Ozs7OztBQzdFcEIsb0JBQVksR0FBRztJQUN4QjtRQUNJLElBQUksRUFBRSxDQUFDO1FBQ1AsY0FBYyxFQUFFLGFBQWE7UUFDN0IsTUFBTSxFQUFFLFFBQVE7UUFDaEIsV0FBVyxFQUFFLGFBQWE7UUFDMUIsWUFBWSxFQUFFO1lBQ1YsU0FBUyxFQUFFLHVCQUF1QjtZQUNsQyxPQUFPLEVBQUUsV0FBVztZQUNwQixXQUFXLEVBQUUsVUFBVTtZQUN2QixjQUFjLEVBQUUsMEJBQTBCO1lBQzFDLE9BQU8sRUFBRSx1QkFBdUI7WUFDaEMsaUJBQWlCLEVBQUUsMkJBQTJCO1lBQzlDLEtBQUssRUFBRSxXQUFXO1NBQ3JCO1FBQ0QsUUFBUSxFQUFFO1lBQ04sY0FBYyxFQUFFLENBQUM7WUFDakIsYUFBYSxFQUFFLEVBQUU7WUFDakIscUJBQXFCLEVBQUU7Z0JBQ25CLEdBQUcsRUFBRSxHQUFHO2dCQUNSLEdBQUcsRUFBRSxHQUFHO2dCQUNSLEdBQUcsRUFBRSxHQUFHO2dCQUNSLEdBQUcsRUFBRSxHQUFHO2dCQUNSLEdBQUcsRUFBRSxHQUFHO2FBQ1g7U0FDSjtRQUNELFdBQVcsRUFBRTtZQUNUO2dCQUNJLE1BQU0sRUFBRSxTQUFTO2dCQUNqQixNQUFNLEVBQUUsMkRBQTJEO2FBQ3RFO1lBQ0Q7Z0JBQ0ksTUFBTSxFQUFFLFNBQVM7Z0JBQ2pCLE1BQU0sRUFBRSwyREFBMkQ7YUFDdEU7U0FDSjtRQUNELE9BQU8sRUFBRTtZQUNMLFlBQVksRUFBRSxVQUFVO1lBQ3hCLGNBQWMsRUFBRSxJQUFJO1lBQ3BCLFNBQVMsRUFBRSxPQUFPO1lBQ2xCLFNBQVMsRUFBRSxRQUFRO1lBQ25CLE9BQU8sRUFBRSxTQUFTO1lBQ2xCLE1BQU0sRUFBRSxPQUFPO1NBQ2xCO0tBQ0o7Q0FDSjs7Ozs7Ozs7Ozs7Ozs7O0FDN0NELG1EQUE2QjtBQUM3QixzREFBK0I7QUFFL0IsNkRBQXdCO0FBRXhCLElBQU0sSUFBSSxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQztBQUNyRCxhQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztBQUN0QixJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQUcsQ0FBQyxDQUFDO0FBQ3RDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDcEIsTUFBTSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFDNUIsTUFBTSxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7QUFJcEMsdUJBQXVCLEdBQW9CO0lBQ3ZDLElBQUksSUFBSSxHQUFXLENBQUMsT0FBTyxHQUFHLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUN2RSxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFBRSxPQUFPLEdBQUcsQ0FBQztTQUN2QixJQUFJLElBQUksSUFBSSxDQUFDO1FBQUUsT0FBTyxJQUFJLENBQUM7O1FBQzNCLE9BQU8sS0FBSyxDQUFDO0FBQ3RCLENBQUM7QUFFRCxpQkFBaUIsS0FBNEI7SUFDekMsSUFBSSxLQUFLLENBQUMsT0FBTyxLQUFLLFFBQVE7UUFBRSxNQUFNLEtBQUssQ0FBQztJQUM1QyxJQUFJLElBQUksR0FBRyxDQUFDLE9BQU8sSUFBSSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0lBQ3hFLFFBQVEsS0FBSyxDQUFDLElBQUksRUFBRTtRQUNoQixLQUFLLFFBQVE7WUFDVCxPQUFPLENBQUMsS0FBSyxDQUFJLElBQUksa0NBQStCLENBQUMsQ0FBQztZQUN0RCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLE1BQU07UUFDVixLQUFLLFlBQVk7WUFDYixPQUFPLENBQUMsS0FBSyxDQUFJLElBQUksdUJBQW9CLENBQUMsQ0FBQztZQUMzQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLE1BQU07UUFDVjtZQUNJLE1BQU0sS0FBSyxDQUFDO0tBQ25CO0FBQ0wsQ0FBQztBQUVEO0lBQ0ksSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzVCLElBQUksSUFBSSxHQUFHLENBQUMsT0FBTyxJQUFJLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVEsSUFBTSxDQUFDLENBQUMsQ0FBQyxVQUFRLElBQUksQ0FBQyxJQUFNLENBQUM7SUFDN0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7SUFDakIsS0FBSyxDQUFDLGtCQUFnQixJQUFNLENBQUMsQ0FBQztBQUNsQyxDQUFDOzs7Ozs7Ozs7Ozs7OztBQzNDRCwwQkFBMEI7O0FBRTFCO0lBQUE7UUFHWSxrQkFBYSxHQUFPO1lBQ3hCLHNCQUFzQixFQUFFO2dCQUNwQixRQUFRLEVBQUU7b0JBQ04sTUFBTSxFQUFFLFdBQVc7b0JBQ25CLE1BQU0sRUFBRSxNQUFNO2lCQUNqQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ0wsTUFBTSxFQUFFLFdBQVc7b0JBQ25CLE1BQU0sRUFBRSxJQUFJO29CQUNaLElBQUksRUFBRSxDQUFDO2lCQUNWO2dCQUNELE9BQU8sRUFBRTtvQkFDTCxTQUFTLEVBQUUsSUFBSTtvQkFDZixpQkFBaUIsRUFBRSxFQUFFO29CQUNyQixlQUFlLEVBQUUsTUFBTTtvQkFDdkIsTUFBTSxFQUFFLDZCQUE2QjtvQkFDckMsTUFBTSxFQUFFLElBQUk7b0JBQ1osTUFBTSxFQUFFLGdCQUFnQjtvQkFDeEIsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLElBQUksRUFBRSx3QkFBd0I7aUJBQ2pDO2FBQ0o7U0FDSixDQUFDO1FBQ00sMEJBQXFCLEdBQVUsNEJBQTRCLENBQUM7SUFvQnhFLENBQUM7SUFsQlUsZ0RBQWlCLEdBQXhCO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO1FBQzNELGtIQUFrSDtRQUNsSCwyREFBMkQ7SUFDL0QsQ0FBQztJQUVNLGtEQUFtQixHQUExQjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUscUJBQXFCLENBQUMsQ0FBQztRQUM3RCxvR0FBb0c7SUFDeEcsQ0FBQztJQUVNLCtDQUFnQixHQUF2QjtRQUNJLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRWEsZ0NBQVcsR0FBekI7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBQ0wsMkJBQUM7QUFBRCxDQUFDO0FBRUQsa0JBQWUsb0JBQW9CLENBQUM7Ozs7Ozs7Ozs7Ozs7OztBQ2xEcEMsOERBQWdFO0FBQ2hFLElBQU0sUUFBUSxHQUFHLG1CQUFPLENBQUMsNENBQWdCLENBQUMsQ0FBQztBQUUzQztJQUdJOztPQUVHO0lBQ0g7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFNLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksZ0NBQVMsR0FBaEIsVUFBaUIsR0FBWSxFQUFFLEdBQWEsRUFBRSxJQUFrQjtRQUU1RCxJQUFJLFNBQVMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUNuQyxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJLFVBQVUsR0FBRztZQUNiLFNBQVMsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU87WUFDM0IsT0FBTyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSztZQUN2QixXQUFXLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTO1NBQ2xDLENBQUM7UUFFRixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSwrQkFBK0IsR0FBRyxTQUFTLEdBQUcsWUFBWSxHQUFHLElBQUksQ0FBQyxDQUFDO1FBRTNGLG9EQUFvRDtRQUVwRCxHQUFHLENBQUMsSUFBSSxDQUFDO1lBQ0wsTUFBTSxFQUFFLElBQUk7U0FDZixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxnQ0FBUyxHQUFoQixVQUFpQixHQUFZLEVBQUUsR0FBYSxFQUFFLElBQWtCO1FBQzVELElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzdDLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFVLElBQUssYUFBTSxDQUFDLEVBQUUsS0FBSyxRQUFRLEVBQXRCLENBQXNCLENBQUMsQ0FBQztRQUNuRSxJQUFJLE1BQU0sRUFBRTtZQUNSLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2lCQUNWLElBQUksQ0FBQztnQkFDRixPQUFPLEVBQUUsU0FBUztnQkFDbEIsTUFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNO2dCQUNsQixNQUFNLEVBQUUsTUFBTTthQUNqQixDQUFDLENBQUM7U0FDVjthQUNJO1lBQ0QsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7aUJBQ1YsSUFBSSxDQUFDO2dCQUNGLE9BQU8sRUFBRSxvQ0FBb0M7Z0JBQzdDLE1BQU0sRUFBRSxHQUFHLENBQUMsTUFBTTthQUNyQixDQUFDLENBQUM7U0FDVjtJQUNMLENBQUM7SUFFRDs7O09BR0c7SUFDSCwyQkFBSSxHQUFKO1FBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFTCxtQkFBQztBQUFELENBQUM7QUFqRVksb0NBQVk7QUFtRXpCLGtFQUFrRTtBQUNsRSxJQUFNLFlBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0FBQ3hDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztBQUVwQixJQUFJLGFBQWEsR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDO0FBRXhDLGtCQUFlLGFBQWEsQ0FBQztBQUM3QiwrQkFBK0I7Ozs7Ozs7Ozs7Ozs7OztBQzdFL0IsOERBQWdFO0FBRWhFO0lBR0U7O09BRUc7SUFDSDtRQUNFLElBQUksQ0FBQyxNQUFNLEdBQUcsZ0JBQU0sRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRDs7T0FFRztJQUNJLHlCQUFJLEdBQVgsVUFBWSxHQUFZLEVBQUUsR0FBYSxFQUFFLElBQWtCO1FBQ3ZELEdBQUcsQ0FBQyxJQUFJLENBQUM7WUFDTCxNQUFNLEVBQUUsSUFBSTtTQUNmLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRDs7O09BR0c7SUFDSCx5QkFBSSxHQUFKO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUgsaUJBQUM7QUFBRCxDQUFDO0FBN0JZLGdDQUFVO0FBK0J2QixrRUFBa0U7QUFDbEUsSUFBTSxVQUFVLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztBQUNwQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDbEIsSUFBSSxXQUFXLEdBQUcsVUFBVSxDQUFDLE1BQU07QUFDbkMsa0JBQWUsV0FBVyxDQUFDO0FBQzNCLDZCQUE2Qjs7Ozs7Ozs7Ozs7Ozs7O0FDdEM3Qiw4REFBZ0U7QUFDaEUsSUFBTSxRQUFRLEdBQUcsbUJBQU8sQ0FBQyw0Q0FBZ0IsQ0FBQyxDQUFDO0FBRTNDO0lBR0k7O09BRUc7SUFDSDtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsZ0JBQU0sRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoQixDQUFDO0lBRUQ7O09BRUc7SUFDSSwyQkFBSSxHQUFYLFVBQVksR0FBWSxFQUFFLEdBQWEsRUFBRSxJQUFrQjtRQUN2RCxJQUFJLE1BQU0sR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUM3QixJQUFJLFFBQVEsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNqQyxJQUFJLFdBQVcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUV2QyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxlQUFlLEdBQUcsTUFBTSxHQUFHLFFBQVEsR0FBRyxXQUFXLEdBQUcsdUJBQXVCLEdBQUcsUUFBUSxDQUFDLENBQUM7UUFFaEgsR0FBRyxDQUFDLElBQUksQ0FBQztZQUNMLE1BQU0sRUFBRSxJQUFJO1NBQ2YsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOztPQUVHO0lBQ0ksZ0NBQVMsR0FBaEIsVUFBaUIsR0FBWSxFQUFFLEdBQWEsRUFBRSxJQUFrQjtRQUM1RCxJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QyxJQUFJLE1BQU0sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBVSxJQUFLLGFBQU0sQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUF0QixDQUFzQixDQUFDLENBQUM7UUFDbkUsSUFBSSxNQUFNLEVBQUU7WUFDUixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztpQkFDVixJQUFJLENBQUM7Z0JBQ0YsT0FBTyxFQUFFLFNBQVM7Z0JBQ2xCLE1BQU0sRUFBRSxHQUFHLENBQUMsTUFBTTtnQkFDbEIsTUFBTSxFQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUM7YUFDM0IsQ0FBQyxDQUFDO1NBQ1Y7YUFDSTtZQUNELEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2lCQUNWLElBQUksQ0FBQztnQkFDRixPQUFPLEVBQUUsb0NBQW9DO2dCQUM3QyxNQUFNLEVBQUUsR0FBRyxDQUFDLE1BQU07YUFDckIsQ0FBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsMkJBQUksR0FBSjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUwsbUJBQUM7QUFBRCxDQUFDO0FBMURZLG9DQUFZO0FBNER6QixrRUFBa0U7QUFDbEUsSUFBTSxZQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztBQUN4QyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDcEIsSUFBSSxhQUFhLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQztBQUV4QyxrQkFBZSxhQUFhLENBQUM7QUFDN0IsK0JBQStCOzs7Ozs7Ozs7Ozs7Ozs7QUNyRS9CLDhEQUFnRTtBQUNoRSxpSUFBbUU7QUFFbkU7SUFHSTs7T0FFRztJQUNIO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxnQkFBTSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNJLDBDQUFNLEdBQWIsVUFBYyxHQUFZLEVBQUUsR0FBYSxFQUFFLElBQWtCO1FBQ3pELDhCQUFvQixDQUFDLFdBQVcsRUFBRSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDekQsR0FBRyxDQUFDLElBQUksQ0FBQztZQUNMLE1BQU0sRUFBRSxJQUFJO1NBQ2YsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUdEOzs7T0FHRztJQUNILHdDQUFJLEdBQUo7UUFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFFTCxnQ0FBQztBQUFELENBQUM7QUE5QlksOERBQXlCO0FBZ0N0QyxrRUFBa0U7QUFDbEUsSUFBSSx5QkFBeUIsR0FBRyxJQUFJLHlCQUF5QixFQUFFLENBQUM7QUFDaEUseUJBQXlCLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDakMsSUFBSSwwQkFBMEIsR0FBRyx5QkFBeUIsQ0FBQyxNQUFNLENBQUM7QUFDbEUsNENBQTRDO0FBQzVDLGtCQUFlLDBCQUEwQixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUN4QzFDLDhEQUFnRTtBQUNoRSxxRUFBMEM7QUFDMUMsNkNBQTBCO0FBQzFCLG1EQUE2QjtBQUc3QjtJQUdFOztPQUVHO0lBQ0g7UUFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFNLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxrQ0FBVyxHQUFsQixVQUFtQixHQUFZLEVBQUUsR0FBYSxFQUFFLElBQWtCO1FBQ2hFLElBQUksSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBR3pDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLFVBQUMsR0FBVSxFQUFFLE1BQXlCLEVBQUUsS0FBdUI7WUFDN0UsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7WUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUM7WUFFcEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztZQUd0RSxFQUFFLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsVUFBVSxHQUFHO2dCQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUM7Z0JBQzFCLElBQUksR0FBRyxFQUFFO29CQUNQLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsR0FBRyxDQUFDLE1BQU0sRUFBQyxDQUFDO29CQUM3RCxNQUFNLEdBQUcsQ0FBQztpQkFDWDtnQkFDRCx5Q0FBeUM7Z0JBQ3pDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO3FCQUNaLElBQUksQ0FBQztvQkFDSixPQUFPLEVBQUUsU0FBUztvQkFDbEIsTUFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNO2lCQUNuQixDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQztJQUVKLENBQUM7SUFHRDs7O09BR0c7SUFDSCwyQkFBSSxHQUFKO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRUgsbUJBQUM7QUFBRCxDQUFDO0FBbkRZLG9DQUFZO0FBcUR6QixrRUFBa0U7QUFDbEUsSUFBTSxZQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztBQUN4QyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDcEIsSUFBSSxhQUFhLEdBQUcsWUFBWSxDQUFDLE1BQU07QUFDdkMsa0JBQWUsYUFBYSxDQUFDOzs7Ozs7Ozs7Ozs7QUMvRDdCLHdDOzs7Ozs7Ozs7OztBQ0FBLGlDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLG9DOzs7Ozs7Ozs7OztBQ0FBLHVDOzs7Ozs7Ozs7OztBQ0FBLCtCOzs7Ozs7Ozs7OztBQ0FBLGlDOzs7Ozs7Ozs7OztBQ0FBLG1DOzs7Ozs7Ozs7OztBQ0FBLGlDIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC50c1wiKTtcbiIsImltcG9ydCBleHByZXNzID0gcmVxdWlyZSgnZXhwcmVzcycpXG5pbXBvcnQgYm9keVBhcnNlciA9IHJlcXVpcmUoJ2JvZHktcGFyc2VyJyk7XG5pbXBvcnQgbG9nZ2VyID0gcmVxdWlyZSgnbW9yZ2FuJyk7XG5pbXBvcnQgY29ycyA9IHJlcXVpcmUoJ2NvcnMnKTtcbmltcG9ydCBDb25maWd1cmF0aW9uTWFuYWdlciBmcm9tICcuL21hbmFnZXIvQ29uZmlndXJhdGlvbk1hbmFnZXInO1xuaW1wb3J0IERCTWFuYWdlciBmcm9tICcuL21hbmFnZXIvREJNYW5hZ2VyJztcbmltcG9ydCBwYXRoID0gcmVxdWlyZSgncGF0aCcpXG5cbmltcG9ydCBQaW5nUm91dGVyIGZyb20gJy4vcm91dGVzL1BpbmdSb3V0ZXInO1xuaW1wb3J0IFJlbG9hZENvbmZpZ3VyYXRpb25Sb3V0ZXIgZnJvbSBcIi4vcm91dGVzL1JlbG9hZENvbmZpZ3VyYXRpb25Sb3V0ZXJcIjtcbmltcG9ydCBSYXRpbmdSb3V0ZXIgZnJvbSBcIi4vcm91dGVzL1JhdGluZ1JvdXRlclwiO1xuaW1wb3J0IEVudGl0eVJvdXRlciBmcm9tIFwiLi9yb3V0ZXMvRW50aXR5Um91dGVyXCI7XG5pbXBvcnQgVXBsb2FkUm91dGVyIGZyb20gXCIuL3JvdXRlcy9VcGxvYWRSb3V0ZXJcIjtcblxuLy8gbGV0IGV4cHJlc3MgPSByZXF1aXJlKCdleHByZXNzJyk7XG5cblxuLy8gQ3JlYXRlcyBhbmQgY29uZmlndXJlcyBhbiBFeHByZXNzSlMgd2ViIHNlcnZlci5cbmNsYXNzIEFwcCB7XG5cbiAgLy8gcmVmIHRvIEV4cHJlc3MgaW5zdGFuY2VcbiAgcHVibGljIGV4cHJlc3M6IGV4cHJlc3MuQXBwbGljYXRpb247XG4gIC8vIHB1YmxpYyBleHByZXNzOmFueTtcblxuICAvL1J1biBjb25maWd1cmF0aW9uIG1ldGhvZHMgb24gdGhlIEV4cHJlc3MgaW5zdGFuY2UuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIGNvbnNvbGUubG9nKFwic3RhcnQgc2VydmVyXCIpXG4gICAgdGhpcy5leHByZXNzID0gZXhwb3J0cyA9IGV4cHJlc3MoKTtcbiAgICB0aGlzLm1pZGRsZXdhcmUoKTtcbiAgICB0aGlzLnJvdXRlcygpO1xuICAgIENvbmZpZ3VyYXRpb25NYW5hZ2VyLmdldEluc3RhbmNlKCkucmVhZENvbmZpZ3VyYXRpb24oKTtcbiAgICAvLyBEQk1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5pbml0KCk7XG4gIH1cblxuICAvLyBDb25maWd1cmUgRXhwcmVzcyBtaWRkbGV3YXJlLlxuICBwcml2YXRlIG1pZGRsZXdhcmUoKTogdm9pZCB7XG4gICAgLy8gdGhpcy5leHByZXNzLnVzZShleHByZXNzLnN0YXRpYyhcIi4vYnVpbGQvZmUtZGlzdFwiKSlcblxuICAgIHRoaXMuZXhwcmVzcy51c2UobG9nZ2VyKCdkZXYnKSk7XG4gICAgdGhpcy5leHByZXNzLnVzZShib2R5UGFyc2VyLmpzb24oKSk7XG4gICAgdGhpcy5leHByZXNzLnVzZShib2R5UGFyc2VyLnVybGVuY29kZWQoe2V4dGVuZGVkOiBmYWxzZX0pKTtcbiAgICB0aGlzLmV4cHJlc3MudXNlKGNvcnMoKSk7XG4gICAgLy8gICAgIHZhciBmcyA9IHJlcXVpcmUoJ2ZzJyk7XG4vLyAgICAgZnMucmVhZGRpcihcIi4vYnVpbGQvZmUtZGlzdFwiLCAoZXJyLCBmaWxlcykgPT4ge1xuLy8gICAgICAgZmlsZXMuZm9yRWFjaChmaWxlID0+IHtcbi8vICAgICAgICAgY29uc29sZS5sb2coZmlsZSk7XG4vLyAgICAgICB9KTtcbi8vICAgICB9KVxuLy8gY29uc29sZS5sb2coZnMuZXhpc3RzU3luYyhfX2Rpcm5hbWUgK1wiL2Fzc2V0c1wiKSlcbiAgfVxuXG4gIC8vIENvbmZpZ3VyZSBBUEkgZW5kcG9pbnRzLlxuICBwcml2YXRlIHJvdXRlcygpOiB2b2lkIHtcbiAgICAvKiBUaGlzIGlzIGp1c3QgdG8gZ2V0IHVwIGFuZCBydW5uaW5nLCBhbmQgdG8gbWFrZSBzdXJlIHdoYXQgd2UndmUgZ290IGlzXG4gICAgICogd29ya2luZyBzbyBmYXIuIFRoaXMgZnVuY3Rpb24gd2lsbCBjaGFuZ2Ugd2hlbiB3ZSBzdGFydCB0byBhZGQgbW9yZVxuICAgICAqIEFQSSBlbmRwb2ludHMgKi9cbiAgICBsZXQgcm91dGVyID0gZXhwcmVzcy5Sb3V0ZXIoKTtcbiAgICAvLyBwbGFjZWhvbGRlciByb3V0ZSBoYW5kbGVyXG4gICAgcm91dGVyLmdldCgnLycsIChyZXE6IGFueSwgcmVzOiBhbnksIG5leHQ6IGFueSkgPT4ge1xuICAgICAgcmVzLmpzb24oe1xuICAgICAgICBtZXNzYWdlOiAnSGVsbG8gV29ybGQhJ1xuICAgICAgfSk7XG4gICAgfSk7XG4gICAgdGhpcy5leHByZXNzLnVzZSgnLycsIHJvdXRlcik7XG4gICAgdGhpcy5leHByZXNzLnVzZSgnL2FwaS92MS9waW5nJywgUGluZ1JvdXRlcik7XG4gICAgdGhpcy5leHByZXNzLnVzZSgnL2FwaS92MS9yZWxvYWRDb25maWd1cmF0aW9uJywgUmVsb2FkQ29uZmlndXJhdGlvblJvdXRlcik7XG4gICAgdGhpcy5leHByZXNzLnVzZSgnL2FwaS92MS9yYXRpbmcnLCBSYXRpbmdSb3V0ZXIpO1xuICAgIHRoaXMuZXhwcmVzcy51c2UoJy9hcGkvdjEvZW50aXR5JywgRW50aXR5Um91dGVyKTtcbiAgICB0aGlzLmV4cHJlc3MudXNlKCcvdXBsb2FkLW1hbmFnZXInLCBVcGxvYWRSb3V0ZXIpO1xuICAgIHRoaXMuZXhwcmVzcy51c2UoJy9wd2EnLCBleHByZXNzLnN0YXRpYyhcIi4vYnVpbGQvZmUtZGlzdFwiKSk7XG4gICAgLy8gdGhpcy5leHByZXNzLnVzZSgnL3B3YScsIFB3YVJvdXRlcik7XG4gICAgY29uc29sZS5sb2coXCIjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMgICBEaXJOYW1lICAgICAjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMgICBcIiwgX19kaXJuYW1lKVxuICAgIC8vIHRoaXMuZXhwcmVzcy51c2UoJy9wd2EnLCBQd2FSb3V0ZXIpXG4gIH1cblxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgQXBwKCkuZXhwcmVzcztcbiIsImV4cG9ydCBjb25zdCBzaG93Y2FzZU1vY2sgPSBbXG4gICAge1xuICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgIFwiY3JlYXRpb25EYXRlXCI6IDE1MjM5NjkzMjMwNzAsXG4gICAgICAgIFwibmFtZVwiOiBcIk15VGVzdFwiLFxuICAgICAgICBcInZhdE51bWJlclwiOiBcIlZBVC0xMjM0MjM0XCIsXG4gICAgICAgIFwic29jaWFsRGF0YVwiOiB7XG4gICAgICAgICAgICBcIndlYnNpdGVcIjogXCJodHRwczovL3d3dy5nb29nbGUuaXRcIixcbiAgICAgICAgICAgIFwicGhvbmVcIjogXCIwNjMxMjMxMjNcIixcbiAgICAgICAgICAgIFwiY2VsbHBob25lXCI6IFwiMzIzMjMyMzJcIixcbiAgICAgICAgICAgIFwiZmFjZWJvb2tMaW5rXCI6IFwiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tXCIsXG4gICAgICAgICAgICBcImVtYWlsXCI6IFwibXl0ZXN0QG1haWxpbmF0b3IuY29tXCIsXG4gICAgICAgICAgICBcInRyaXBhZHZpc29yTGlua1wiOiBcImh0dHA6Ly93d3cudHJpcGFkdmlzb3IuaXRcIixcbiAgICAgICAgICAgIFwiZmF4XCI6IFwiMTIzMTIzMTIzXCJcbiAgICAgICAgfSxcbiAgICAgICAgXCJyYXRpbmdcIjoge1xuICAgICAgICAgICAgXCJhdmVyYWdlVmFsdWVcIjogNCxcbiAgICAgICAgICAgIFwicmF0aW5nQ291bnRcIjogMTAsXG4gICAgICAgICAgICBcInJldmlld19yYXRpbmdfY291bnRcIjoge1xuICAgICAgICAgICAgICAgIFwiMVwiOiBcIjBcIixcbiAgICAgICAgICAgICAgICBcIjJcIjogXCIxXCIsXG4gICAgICAgICAgICAgICAgXCIzXCI6IFwiM1wiLFxuICAgICAgICAgICAgICAgIFwiNFwiOiBcIjFcIixcbiAgICAgICAgICAgICAgICBcIjVcIjogXCI1XCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgXCJpbWFnZUxpc3RcIjogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInBob3RvLTFcIixcbiAgICAgICAgICAgICAgICBcInBhdGhcIjogXCJodHRwOi8vd3d3LnNhbXBsZS12aWRlb3MuY29tL2ltZy9TYW1wbGUtanBnLWltYWdlLTFtYi5qcGdcIlxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJwaG90by0yXCIsXG4gICAgICAgICAgICAgICAgXCJwYXRoXCI6IFwiaHR0cDovL3d3dy5zYW1wbGUtdmlkZW9zLmNvbS9pbWcvU2FtcGxlLWpwZy1pbWFnZS0xbWIuanBnXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgXCJwbGFjZVwiOiB7XG4gICAgICAgICAgICBcInN0cmVldE5hbWVcIjogXCJ2aWEgUm9tYVwiLFxuICAgICAgICAgICAgXCJzdHJlZXROdW1iZXJcIjogXCIxMlwiLFxuICAgICAgICAgICAgXCJ6aXBDb2RlXCI6IFwiMDEwMTBcIixcbiAgICAgICAgICAgIFwiY291bnRyeVwiOiBcIkl0YWxpYVwiLFxuICAgICAgICAgICAgXCJzdGF0ZVwiOiBcIlZpdGVyYm9cIixcbiAgICAgICAgICAgIFwiY2l0eVwiOiBcIkxhemlvXCJcbiAgICAgICAgfVxuICAgIH1cbl1cbiIsImltcG9ydCAqIGFzIGh0dHAgZnJvbSAnaHR0cCc7XG5pbXBvcnQgKiBhcyBkZWJ1ZyBmcm9tICdkZWJ1Zyc7XG5cbmltcG9ydCBBcHAgZnJvbSAnLi9BcHAnO1xuXG5jb25zdCBwb3J0ID0gbm9ybWFsaXplUG9ydChwcm9jZXNzLmVudi5QT1JUIHx8IDMwMDApO1xuQXBwLnNldCgncG9ydCcsIHBvcnQpO1xuY29uc3Qgc2VydmVyID0gaHR0cC5jcmVhdGVTZXJ2ZXIoQXBwKTtcbnNlcnZlci5saXN0ZW4ocG9ydCk7XG5zZXJ2ZXIub24oJ2Vycm9yJywgb25FcnJvcik7XG5zZXJ2ZXIub24oJ2xpc3RlbmluZycsIG9uTGlzdGVuaW5nKTtcblxuXG5cbmZ1bmN0aW9uIG5vcm1hbGl6ZVBvcnQodmFsOiBudW1iZXIgfCBzdHJpbmcpOiBudW1iZXIgfCBzdHJpbmcgfCBib29sZWFuIHtcbiAgICBsZXQgcG9ydDogbnVtYmVyID0gKHR5cGVvZiB2YWwgPT09ICdzdHJpbmcnKSA/IHBhcnNlSW50KHZhbCwgMTApIDogdmFsO1xuICAgIGlmIChpc05hTihwb3J0KSkgcmV0dXJuIHZhbDtcbiAgICBlbHNlIGlmIChwb3J0ID49IDApIHJldHVybiBwb3J0O1xuICAgIGVsc2UgcmV0dXJuIGZhbHNlO1xufVxuXG5mdW5jdGlvbiBvbkVycm9yKGVycm9yOiBOb2RlSlMuRXJybm9FeGNlcHRpb24pOiB2b2lkIHtcbiAgICBpZiAoZXJyb3Iuc3lzY2FsbCAhPT0gJ2xpc3RlbicpIHRocm93IGVycm9yO1xuICAgIGxldCBiaW5kID0gKHR5cGVvZiBwb3J0ID09PSAnc3RyaW5nJykgPyAnUGlwZSAnICsgcG9ydCA6ICdQb3J0ICcgKyBwb3J0O1xuICAgIHN3aXRjaCAoZXJyb3IuY29kZSkge1xuICAgICAgICBjYXNlICdFQUNDRVMnOlxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihgJHtiaW5kfSByZXF1aXJlcyBlbGV2YXRlZCBwcml2aWxlZ2VzYCk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnRUFERFJJTlVTRSc6XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGAke2JpbmR9IGlzIGFscmVhZHkgaW4gdXNlYCk7XG4gICAgICAgICAgICBwcm9jZXNzLmV4aXQoMSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHRocm93IGVycm9yO1xuICAgIH1cbn1cblxuZnVuY3Rpb24gb25MaXN0ZW5pbmcoKTogdm9pZCB7XG4gICAgbGV0IGFkZHIgPSBzZXJ2ZXIuYWRkcmVzcygpO1xuICAgIGxldCBiaW5kID0gKHR5cGVvZiBhZGRyID09PSAnc3RyaW5nJykgPyBgcGlwZSAke2FkZHJ9YCA6IGBwb3J0ICR7YWRkci5wb3J0fWA7XG4gICAgY29uc29sZS5sb2coYmluZClcbiAgICBkZWJ1ZyhgTGlzdGVuaW5nIG9uICR7YmluZH1gKTtcbn1cbiIsIi8vaW1wb3J0ICogYXMgZnMgZnJvbSAnZnMnXG5cbmNsYXNzIENvbmZpZ3VyYXRpb25NYW5hZ2Vye1xuICAgIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOkNvbmZpZ3VyYXRpb25NYW5hZ2VyO1xuXG4gICAgcHJpdmF0ZSBjb25maWd1cmF0aW9uOmFueSA9IHtcbiAgICAgICAgXCJzdGFydHVwQ29uZmlndXJhdGlvblwiOiB7XG4gICAgICAgICAgICBcInNlcnZlclwiOiB7XG4gICAgICAgICAgICAgICAgXCJob3N0XCI6IFwiMTI3LjAuMC4xXCIsXG4gICAgICAgICAgICAgICAgXCJwb3J0XCI6IFwiODAwMFwiXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgXCJyZWRpc1wiOiB7XG4gICAgICAgICAgICAgICAgXCJob3N0XCI6IFwibG9jYWxob3N0XCIsXG4gICAgICAgICAgICAgICAgXCJwb3J0XCI6IDYzNzksXG4gICAgICAgICAgICAgICAgXCJkYlwiOiA1XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgXCJteXNxbFwiOiB7XG4gICAgICAgICAgICAgICAgXCJlbmFibGVkXCI6IHRydWUsXG4gICAgICAgICAgICAgICAgXCJjb25uZWN0aW9uTGltaXRcIjogMTAsXG4gICAgICAgICAgICAgICAgXCJrZWVwQWxpdmVUaW1lXCI6IDYwMDAwMCxcbiAgICAgICAgICAgICAgICBcImhvc3RcIjogXCJldS1jZGJyLXdlc3QtMDIuY2xlYXJkYi5uZXRcIixcbiAgICAgICAgICAgICAgICBcInBvcnRcIjogMzMwNixcbiAgICAgICAgICAgICAgICBcInVzZXJcIjogXCJiY2FlNWJkNmQ2NGMxMFwiLFxuICAgICAgICAgICAgICAgIFwicGFzc3dvcmRcIjogXCI2N2JjOTNiNFwiLFxuICAgICAgICAgICAgICAgIFwiZGJcIjogXCJoZXJva3VfNTNiMTc5ODdjMTZlOGMwXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG4gICAgcHJpdmF0ZSBjb25maWd1cmF0aW9uRmlsZVBhdGg6c3RyaW5nID0gXCIuLi9jb25maWd1cmF0aW9uL2NvbmYuanNvblwiO1xuXG4gICAgcHVibGljIHJlYWRDb25maWd1cmF0aW9uKCk6IHZvaWQge1xuICAgICAgICBjb25zb2xlLmxvZyhcIltDb25maWd1cmF0aW9uTWFuYWdlcl1cIiwgXCJyZWFkQ29uZmlndXJhdGlvblwiKTtcbiAgICAgICAgLy9pZih0aGlzLmNvbmZpZ3VyYXRpb25GaWxlUGF0aCA9PSB1bmRlZmluZWQpe2NvbnNvbGUubG9nKG5ldyBEYXRlKCkgKyBcIiB8IFtDb25maWd1cmF0aW9uTWFuYWdlcl0gbm8gY29uZiBwYXRoXCIpO31cbiAgICAgICAgLy90aGlzLmNvbmZpZ3VyYXRpb24gPSByZXF1aXJlKHRoaXMuY29uZmlndXJhdGlvbkZpbGVQYXRoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgcmVsb2FkQ29uZmlndXJhdGlvbigpOiB2b2lkIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJbQ29uZmlndXJhdGlvbk1hbmFnZXJdXCIsIFwicmVsb2FkQ29uZmlndXJhdGlvblwiKTtcbiAgICAgICAgLy90aGlzLmNvbmZpZ3VyYXRpb24gPSBKU09OLnBhcnNlKGZzLnJlYWRGaWxlU3luYyhcIi4uL2NvbmZpZ3VyYXRpb24vY29uZmlnLmpzb25cIiwgbnVsbCkudG9TdHJpbmcoKSk7XG4gICAgfVxuXG4gICAgcHVibGljIGdldENvbmZpZ3VyYXRpb24oKTogYW55e1xuICAgICAgICByZXR1cm4gdGhpcy5jb25maWd1cmF0aW9uO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0SW5zdGFuY2UoKXtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2UgfHwgKHRoaXMuaW5zdGFuY2UgPSBuZXcgdGhpcygpKTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IENvbmZpZ3VyYXRpb25NYW5hZ2VyOyIsImltcG9ydCB7Um91dGVyLCBSZXF1ZXN0LCBSZXNwb25zZSwgTmV4dEZ1bmN0aW9ufSBmcm9tICdleHByZXNzJztcbmNvbnN0IEVudGl0aWVzID0gcmVxdWlyZSgnLi4vYXNzZXRzL2RhdGEnKTtcblxuZXhwb3J0IGNsYXNzIEVudGl0eVJvdXRlciB7XG4gICAgcm91dGVyOiBSb3V0ZXJcblxuICAgIC8qKlxuICAgICAqIEluaXRpYWxpemUgdGhlIEVudGl0eVJvdXRlclxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLnJvdXRlciA9IFJvdXRlcigpO1xuICAgICAgICB0aGlzLmluaXQoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBQT1NUIG5ldyBFbnRpdHkuXG4gICAgICovXG4gICAgcHVibGljIGFkZEVudGl0eShyZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UsIG5leHQ6IE5leHRGdW5jdGlvbikge1xuXG4gICAgICAgIGxldCB2YXROdW1iZXIgPSByZXEuYm9keS52YXROdW1iZXI7XG4gICAgICAgIGxldCBuYW1lID0gcmVxLmJvZHkubmFtZTtcbiAgICAgICAgbGV0IHNvY2lhbERhdGEgPSB7XG4gICAgICAgICAgICBcIndlYnNpdGVcIjogcmVxLmJvZHkud2Vic2l0ZSxcbiAgICAgICAgICAgIFwicGhvbmVcIjogcmVxLmJvZHkucGhvbmUsXG4gICAgICAgICAgICBcImNlbGxwaG9uZVwiOiByZXEuYm9keS5jZWxscGhvbmVcbiAgICAgICAgfTtcblxuICAgICAgICBjb25zb2xlLmxvZyhcIltSYXRpbmddXCIsIFwiYWRkaW5nIGVudGl0eSB3aXRoIHZhdE51bWJlcj1cIiArIHZhdE51bWJlciArIFwiIGFuZCBuYW1lPVwiICsgbmFtZSk7XG5cbiAgICAgICAgLy9jaGVjayBpZiBhbHJlZHkgZXhpc3RzIGVudGl0eSB3aXRoIGdpdmVuIHZhdE51bWJlclxuXG4gICAgICAgIHJlcy5qc29uKHtcbiAgICAgICAgICAgIHN0YXR1czogJ29rJ1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHRVQgb25lIGVudGl0eSBieSBpZFxuICAgICAqL1xuICAgIHB1YmxpYyBnZXRFbnRpdHkocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlLCBuZXh0OiBOZXh0RnVuY3Rpb24pIHtcbiAgICAgICAgbGV0IGVudGl0eUlkID0gcGFyc2VJbnQocmVxLnBhcmFtcy5lbnRpdHlJZCk7XG4gICAgICAgIGxldCBlbnRpdHkgPSBFbnRpdGllcy5maW5kKChlbnRpdHk6YW55KSA9PiBlbnRpdHkuaWQgPT09IGVudGl0eUlkKTtcbiAgICAgICAgaWYgKGVudGl0eSkge1xuICAgICAgICAgICAgcmVzLnN0YXR1cygyMDApXG4gICAgICAgICAgICAgICAgLnNlbmQoe1xuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnU3VjY2VzcycsXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogcmVzLnN0YXR1cyxcbiAgICAgICAgICAgICAgICAgICAgZW50aXR5OiBlbnRpdHlcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJlcy5zdGF0dXMoNDA0KVxuICAgICAgICAgICAgICAgIC5zZW5kKHtcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ05vIGVudGl0eSBmb3VuZCB3aXRoIHRoZSBnaXZlbiBpZC4nLFxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IHJlcy5zdGF0dXNcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFRha2UgZWFjaCBoYW5kbGVyLCBhbmQgYXR0YWNoIHRvIG9uZSBvZiB0aGUgRXhwcmVzcy5Sb3V0ZXInc1xuICAgICAqIGVuZHBvaW50cy5cbiAgICAgKi9cbiAgICBpbml0KCkge1xuICAgICAgICB0aGlzLnJvdXRlci5wb3N0KCcvJywgdGhpcy5hZGRFbnRpdHkpO1xuICAgICAgICB0aGlzLnJvdXRlci5nZXQoJy86ZW50aXR5SWQnLCB0aGlzLmdldEVudGl0eSk7XG4gICAgfVxuXG59XG5cbi8vIENyZWF0ZSB0aGUgSGVyb1JvdXRlciwgYW5kIGV4cG9ydCBpdHMgY29uZmlndXJlZCBFeHByZXNzLlJvdXRlclxuY29uc3QgZW50aXR5Um91dGVyID0gbmV3IEVudGl0eVJvdXRlcigpO1xuZW50aXR5Um91dGVyLmluaXQoKTtcblxubGV0IF9lbnRpdHlSb3V0ZXIgPSBlbnRpdHlSb3V0ZXIucm91dGVyO1xuXG5leHBvcnQgZGVmYXVsdCBfZW50aXR5Um91dGVyO1xuLy8gZXhwb3J0IGRlZmF1bHQgZW50aXR5Um91dGVyO1xuIiwiaW1wb3J0IHtSb3V0ZXIsIFJlcXVlc3QsIFJlc3BvbnNlLCBOZXh0RnVuY3Rpb259IGZyb20gJ2V4cHJlc3MnO1xuXG5leHBvcnQgY2xhc3MgUGluZ1JvdXRlciB7XG4gIHJvdXRlcjogUm91dGVyXG5cbiAgLyoqXG4gICAqIEluaXRpYWxpemUgdGhlIEhlcm9Sb3V0ZXJcbiAgICovXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMucm91dGVyID0gUm91dGVyKCk7XG4gICAgdGhpcy5pbml0KCk7XG4gIH1cblxuICAvKipcbiAgICogR0VUIGFsbCBIZXJvZXMuXG4gICAqL1xuICBwdWJsaWMgcGluZyhyZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UsIG5leHQ6IE5leHRGdW5jdGlvbikge1xuICAgICAgcmVzLmpzb24oe1xuICAgICAgICAgIHN0YXR1czogJ29rJ1xuICAgICAgfSk7XG4gIH1cblxuXG4gIC8qKlxuICAgKiBUYWtlIGVhY2ggaGFuZGxlciwgYW5kIGF0dGFjaCB0byBvbmUgb2YgdGhlIEV4cHJlc3MuUm91dGVyJ3NcbiAgICogZW5kcG9pbnRzLlxuICAgKi9cbiAgaW5pdCgpIHtcbiAgICB0aGlzLnJvdXRlci5nZXQoJy8nLCB0aGlzLnBpbmcpO1xuICB9XG5cbn1cblxuLy8gQ3JlYXRlIHRoZSBIZXJvUm91dGVyLCBhbmQgZXhwb3J0IGl0cyBjb25maWd1cmVkIEV4cHJlc3MuUm91dGVyXG5jb25zdCBwaW5nUm91dGVzID0gbmV3IFBpbmdSb3V0ZXIoKTtcbnBpbmdSb3V0ZXMuaW5pdCgpO1xubGV0IF9waW5nUm91dGVzID0gcGluZ1JvdXRlcy5yb3V0ZXJcbmV4cG9ydCBkZWZhdWx0IF9waW5nUm91dGVzO1xuLy8gZXhwb3J0IGRlZmF1bHQgcGluZ1JvdXRlcztcbiIsImltcG9ydCB7Um91dGVyLCBSZXF1ZXN0LCBSZXNwb25zZSwgTmV4dEZ1bmN0aW9ufSBmcm9tICdleHByZXNzJztcbmNvbnN0IEVudGl0aWVzID0gcmVxdWlyZSgnLi4vYXNzZXRzL2RhdGEnKTtcblxuZXhwb3J0IGNsYXNzIFJhdGluZ1JvdXRlciB7XG4gICAgcm91dGVyOiBSb3V0ZXJcblxuICAgIC8qKlxuICAgICAqIEluaXRpYWxpemUgdGhlIEhlcm9Sb3V0ZXJcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5yb3V0ZXIgPSBSb3V0ZXIoKTtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR0VUIGFsbCBIZXJvZXMuXG4gICAgICovXG4gICAgcHVibGljIHJhdGUocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlLCBuZXh0OiBOZXh0RnVuY3Rpb24pIHtcbiAgICAgICAgbGV0IHVzZXJJZCA9IHJlcS5ib2R5LnVzZXJJZDtcbiAgICAgICAgbGV0IGVudGl0eUlkID0gcmVxLmJvZHkuZW50aXR5SWQ7XG4gICAgICAgIGxldCByYXRpbmdWYWx1ZSA9IHJlcS5ib2R5LnJhdGluZ1ZhbHVlO1xuXG4gICAgICAgIGNvbnNvbGUubG9nKFwiW1JhdGluZ11cIiwgXCJVc2VyIHdpdGggaWQ9XCIgKyB1c2VySWQgKyBcIiByYXRlIFwiICsgcmF0aW5nVmFsdWUgKyBcIiBzdGFyIGVudGl0eSB3aXRoIGlkPVwiICsgZW50aXR5SWQpO1xuXG4gICAgICAgIHJlcy5qc29uKHtcbiAgICAgICAgICAgIHN0YXR1czogJ29rJ1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHRVQgb25lIGhlcm8gYnkgaWRcbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0UmF0aW5nKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSwgbmV4dDogTmV4dEZ1bmN0aW9uKSB7XG4gICAgICAgIGxldCBlbnRpdHlJZCA9IHBhcnNlSW50KHJlcS5wYXJhbXMuZW50aXR5SWQpO1xuICAgICAgICBsZXQgZW50aXR5ID0gRW50aXRpZXMuZmluZCgoZW50aXR5OmFueSkgPT4gZW50aXR5LmlkID09PSBlbnRpdHlJZCk7XG4gICAgICAgIGlmIChlbnRpdHkpIHtcbiAgICAgICAgICAgIHJlcy5zdGF0dXMoMjAwKVxuICAgICAgICAgICAgICAgIC5zZW5kKHtcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ1N1Y2Nlc3MnLFxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IHJlcy5zdGF0dXMsXG4gICAgICAgICAgICAgICAgICAgIGVudGl0eTogZW50aXR5W1wicmF0aW5nXCJdXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXMuc3RhdHVzKDQwNClcbiAgICAgICAgICAgICAgICAuc2VuZCh7XG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdObyBlbnRpdHkgZm91bmQgd2l0aCB0aGUgZ2l2ZW4gaWQuJyxcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiByZXMuc3RhdHVzXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUYWtlIGVhY2ggaGFuZGxlciwgYW5kIGF0dGFjaCB0byBvbmUgb2YgdGhlIEV4cHJlc3MuUm91dGVyJ3NcbiAgICAgKiBlbmRwb2ludHMuXG4gICAgICovXG4gICAgaW5pdCgpIHtcbiAgICAgICAgdGhpcy5yb3V0ZXIucG9zdCgnLycsIHRoaXMucmF0ZSk7XG4gICAgICAgIHRoaXMucm91dGVyLmdldCgnLzplbnRpdHlJZCcsIHRoaXMuZ2V0UmF0aW5nKTtcbiAgICB9XG5cbn1cblxuLy8gQ3JlYXRlIHRoZSBIZXJvUm91dGVyLCBhbmQgZXhwb3J0IGl0cyBjb25maWd1cmVkIEV4cHJlc3MuUm91dGVyXG5jb25zdCByYXRpbmdSb3V0ZXIgPSBuZXcgUmF0aW5nUm91dGVyKCk7XG5yYXRpbmdSb3V0ZXIuaW5pdCgpO1xubGV0IF9yYXRpbmdSb3V0ZXIgPSByYXRpbmdSb3V0ZXIucm91dGVyO1xuXG5leHBvcnQgZGVmYXVsdCBfcmF0aW5nUm91dGVyO1xuLy8gZXhwb3J0IGRlZmF1bHQgcmF0aW5nUm91dGVyO1xuIiwiaW1wb3J0IHtSb3V0ZXIsIFJlcXVlc3QsIFJlc3BvbnNlLCBOZXh0RnVuY3Rpb259IGZyb20gJ2V4cHJlc3MnO1xuaW1wb3J0IENvbmZpZ3VyYXRpb25NYW5hZ2VyIGZyb20gJy4uL21hbmFnZXIvQ29uZmlndXJhdGlvbk1hbmFnZXInO1xuXG5leHBvcnQgY2xhc3MgUmVsb2FkQ29uZmlndXJhdGlvblJvdXRlciB7XG4gICAgcm91dGVyOiBSb3V0ZXI7XG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplIHRoZSBIZXJvUm91dGVyXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMucm91dGVyID0gUm91dGVyKCk7XG4gICAgICAgIHRoaXMuaW5pdCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdFVCBhbGwgSGVyb2VzLlxuICAgICAqL1xuICAgIHB1YmxpYyByZWxvYWQocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlLCBuZXh0OiBOZXh0RnVuY3Rpb24pIHtcbiAgICAgICAgQ29uZmlndXJhdGlvbk1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5yZWxvYWRDb25maWd1cmF0aW9uKCk7XG4gICAgICAgIHJlcy5qc29uKHtcbiAgICAgICAgICAgIHN0YXR1czogJ29rJ1xuICAgICAgICB9KTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIFRha2UgZWFjaCBoYW5kbGVyLCBhbmQgYXR0YWNoIHRvIG9uZSBvZiB0aGUgRXhwcmVzcy5Sb3V0ZXInc1xuICAgICAqIGVuZHBvaW50cy5cbiAgICAgKi9cbiAgICBpbml0KCkge1xuICAgICAgICB0aGlzLnJvdXRlci5nZXQoJy8nLCB0aGlzLnJlbG9hZCk7XG4gICAgfVxuXG59XG5cbi8vIENyZWF0ZSB0aGUgSGVyb1JvdXRlciwgYW5kIGV4cG9ydCBpdHMgY29uZmlndXJlZCBFeHByZXNzLlJvdXRlclxubGV0IHJlbG9hZENvbmZpZ3VyYXRpb25Sb3V0ZXIgPSBuZXcgUmVsb2FkQ29uZmlndXJhdGlvblJvdXRlcigpO1xucmVsb2FkQ29uZmlndXJhdGlvblJvdXRlci5pbml0KCk7XG5sZXQgX3JlbG9hZENvbmZpZ3VyYXRpb25Sb3V0ZXIgPSByZWxvYWRDb25maWd1cmF0aW9uUm91dGVyLnJvdXRlcjtcbi8vIGV4cG9ydCBkZWZhdWx0IHJlbG9hZENvbmZpZ3VyYXRpb25Sb3V0ZXI7XG5leHBvcnQgZGVmYXVsdCBfcmVsb2FkQ29uZmlndXJhdGlvblJvdXRlcjtcbiIsImltcG9ydCB7Um91dGVyLCBSZXF1ZXN0LCBSZXNwb25zZSwgTmV4dEZ1bmN0aW9ufSBmcm9tICdleHByZXNzJztcbmltcG9ydCBmb3JtaWRhYmxlID0gcmVxdWlyZShcImZvcm1pZGFibGVcIik7XG5pbXBvcnQgZnMgPSByZXF1aXJlKFwiZnNcIik7XG5pbXBvcnQgcGF0aCA9IHJlcXVpcmUoJ3BhdGgnKVxuXG5cbmV4cG9ydCBjbGFzcyBVcGxvYWRSb3V0ZXIge1xuICByb3V0ZXI6IFJvdXRlclxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplIHRoZSBIZXJvUm91dGVyXG4gICAqL1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnJvdXRlciA9IFJvdXRlcigpO1xuICAgIHRoaXMuaW5pdCgpO1xuICB9XG5cbiAgLyoqXG4gICAqIEdFVCBhbGwgSGVyb2VzLlxuICAgKi9cbiAgcHVibGljIHVwbG9hZEltYWdlKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSwgbmV4dDogTmV4dEZ1bmN0aW9uKSB7XG4gICAgbGV0IGZvcm0gPSBuZXcgZm9ybWlkYWJsZS5JbmNvbWluZ0Zvcm0oKTtcblxuXG4gICAgZm9ybS5wYXJzZShyZXEsIChlcnI6IEVycm9yLCBmaWVsZHM6IGZvcm1pZGFibGUuRmllbGRzLCBmaWxlczogZm9ybWlkYWJsZS5GaWxlcykgPT4ge1xuICAgICAgdmFyIG9sZHBhdGggPSBmaWxlcy52aWRlby5wYXRoO1xuICAgICAgY29uc29sZS5sb2cob2xkcGF0aClcblxuICAgICAgdmFyIG5ld3BhdGggPSBwYXRoLnJlc29sdmUoJy4vdXBsb2FkRm9sZGVyJykgKyBcIi9cIiArIGZpbGVzLnZpZGVvLm5hbWU7XG5cblxuICAgICAgZnMucmVuYW1lKG9sZHBhdGgsIG5ld3BhdGgsIGZ1bmN0aW9uIChlcnIpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciBcIiwgZXJyKVxuICAgICAgICBpZiAoZXJyKSB7XG4gICAgICAgICAgcmVzLnN0YXR1cyg1MDApLnNlbmQoe21lc3NhZ2U6ICdGYWlsZWQnLCBzdGF0dXM6IHJlcy5zdGF0dXN9KVxuICAgICAgICAgIHRocm93IGVycjtcbiAgICAgICAgfVxuICAgICAgICAvLyByZXMud3JpdGUoJ0ZpbGUgdXBsb2FkZWQgYW5kIG1vdmVkIScpO1xuICAgICAgICByZXMuc3RhdHVzKDIwMClcbiAgICAgICAgICAuc2VuZCh7XG4gICAgICAgICAgICBtZXNzYWdlOiAnU3VjY2VzcycsXG4gICAgICAgICAgICBzdGF0dXM6IHJlcy5zdGF0dXNcbiAgICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pXG5cbiAgfVxuXG5cbiAgLyoqXG4gICAqIFRha2UgZWFjaCBoYW5kbGVyLCBhbmQgYXR0YWNoIHRvIG9uZSBvZiB0aGUgRXhwcmVzcy5Sb3V0ZXInc1xuICAgKiBlbmRwb2ludHMuXG4gICAqL1xuICBpbml0KCkge1xuICAgIHRoaXMucm91dGVyLnBvc3QoJy9pbWFnZScsIHRoaXMudXBsb2FkSW1hZ2UpO1xuICB9XG5cbn1cblxuLy8gQ3JlYXRlIHRoZSBIZXJvUm91dGVyLCBhbmQgZXhwb3J0IGl0cyBjb25maWd1cmVkIEV4cHJlc3MuUm91dGVyXG5jb25zdCB1cGxvYWRSb3V0ZXIgPSBuZXcgVXBsb2FkUm91dGVyKCk7XG51cGxvYWRSb3V0ZXIuaW5pdCgpO1xubGV0IF91cGxvYWRSb3V0ZXIgPSB1cGxvYWRSb3V0ZXIucm91dGVyXG5leHBvcnQgZGVmYXVsdCBfdXBsb2FkUm91dGVyO1xuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYm9keS1wYXJzZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiY29yc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJkZWJ1Z1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJleHByZXNzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImZvcm1pZGFibGVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZnNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiaHR0cFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJtb3JnYW5cIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicGF0aFwiKTsiXSwic291cmNlUm9vdCI6IiJ9